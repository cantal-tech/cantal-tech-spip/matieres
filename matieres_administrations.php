<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

// Installation
function matieres_upgrade($nom_meta_base_version, $version_cible) {

    include_spip('base/upgrade');

    $maj = array();

    $maj['create'] = array(
        array('maj_tables',
            array(
                'spip_matieres',
                'spip_matieres_liens'
            )
        ),
    );

    maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

// Desinstallation
function matieres_vider_tables($nom_meta_base_version){

    sql_drop_table("spip_matieres");
    sql_drop_table("spip_matieres_liens");

    effacer_meta($nom_meta_base_version);
}
