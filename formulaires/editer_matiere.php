<?php


if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('inc/actions');
include_spip('inc/editer');
include_spip('inc/modifier');


function formulaires_editer_matiere_saisies( $objet, $id_objet ){

    $saisies = array(
        array(
            'saisie' => 'input',
            'options' => array(
                'nom' => 'titre',
                'label' => 'titre'
            )
        )
    );

    return $saisies;
}

function formulaires_editer_matiere_charger($id_matiere='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
    return formulaires_editer_objet_charger('matiere',$id_matiere,'',$lier_trad,$retour,$config_fonc,$row,$hidden);
}

function formulaires_editer_matiere_verifier($id_matiere='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
    return formulaires_editer_objet_verifier('matiere',$id_matiere,'',$lier_trad,$retour,$config_fonc,$row,$hidden);
}

function formulaires_editer_matiere_traiter($id_matiere='new', $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){
    return formulaires_editer_objet_traiter('matiere',$id_matiere,'',$lier_trad,$retour,$config_fonc,$row,$hidden);
}