<?php
/* Formulaire de liaisons des matieres */


if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_lier_matieres_charger($objet, $id_objet){

    $liaisons = array();
    $matieres = array();
    $in = array();

    # Preparer le IN a exlcure
    if ( $liaisons = get_liaisons( $objet, $id_objet ) )
        foreach ( $liaisons as $k => $v )
            $in[] = $v['id_matiere'];

    # Lister les matieres
    # générer le data pour le select de saisies et exlcure les matières déja liées
    if ( $req = sql_allfetsel('titre, id_matiere', 'spip_matieres', sql_in('id_matiere',$in,'NOT')) )
        foreach ( $req as $k => $v )
            $matieres[$v['id_matiere']] = $v['titre'];

    return array(
        'liaisons' => $liaisons,
        'matieres' => $matieres,
        'idModif' => ''
    );
}

function formulaires_lier_matieres_verifier($objet, $id_objet){

    $erreurs = array();

    # Verifier désassocier

    # Verifier associer

    return $erreurs;
}

function formulaires_lier_matieres_traiter($objet, $id_objet){

    # Utiliser api lien (cf: https://www.spip.net/fr_article5477.html)
    include_spip('action/editer_liens');

    $desassocier    = _request('desassocier');
    $associer       = _request('associer');
    $matiere        = _request('matiere');      // Todo: rennomer en qqc de générique
    $liaisonsSaisies= _request('pourcentage');  // Todo: rennomer en qqc de générique pour tous les champs qualifiables
    $liaisonsBdd    = array();
    $idModif  = array();

    $retour = array();

    # Desassocier
    if ( $desassocier && !$associer ){
        $desassocier = explode('-',$desassocier);
        if ( $res = objet_dissocier(array("matiere"=>$desassocier[2]), array($desassocier[0]=>$desassocier[1])) )
            $retour['message_ok'] = ''; // un message?
        else
            $retour['message_erreur'] = _T('matiere:erreur_matiere_desassocier');
    }

    # Associer la matiere et éventuellement les valeurs saisies
    if ( $associer && is_array($matiere) && count($matiere) == 2 && $matiere['id_matiere'] != 0 && !$desassocier ){
       if ( objet_associer(
            array("matiere"=>$matiere['id_matiere']),
            array($objet=>$id_objet),
            array('pourcentage'=>$matiere['pourcentage']) )
       ){
            $idModif[] = $matiere['id_matiere'];
       }
    }
    elseif ( !$desassocier && isset($matiere['id_matiere']) &&  $matiere['id_matiere'] == 0 ) {

        # Comparer les liaisons saisies avec les liaisons courantes
        if ( $liaisons = get_liaisons( $objet, $id_objet) ){

            foreach ( $liaisons as $k => $v )
                $liaisonsBdd[$v['id_matiere']] = $v['pourcentage'];

            # Y a t'il une difference? Si oui enregitrer
            # Attention les retours d'api sont buggués retourne 0 alors qu'il y a bien modification sur qualifier ??
            if ( $diff = array_diff( $liaisonsSaisies, $liaisonsBdd) )
                foreach ( $diff as $k => $v )
                    if ( objet_associer( array('matiere'=>$k), array($objet=>intval($id_objet)), array('pourcentage'=>$v)) === 0 )
                        $idModif[] = $k;
        }
    }

    # Retourner les id modifies
    if ( !empty($idModif) ){
        set_request('idModif',implode(",",$idModif)); // notice si on retourne un tableau??
    }

    return $retour;
}

/*
 * Fonction a rendre générique pour tous de lien objet
 *
 * Todo: renvoyer un array avec 2 entrée:
 * la première entré est un tableau issu de la requête
 * la 2e un tableau d'id_objet lié pour calculer un sql_in
 *
 * return array
 */
function get_liaisons( $objet, $id_objet){

    if ( !$objet || !$id_objet ) return;

    $where = array( "objet='$objet'", "id_objet=" . intval($id_objet) );

    if ( $liaisons  = sql_allfetsel(
        'objet, id_objet, matieres_liens.id_matiere as id_matiere, matieres.titre as titre, pourcentage',
        'spip_matieres_liens as matieres_liens LEFT JOIN spip_matieres AS matieres ON ( matieres_liens.id_matiere = matieres.id_matiere )',
        $where )
    )
        return $liaisons;
}