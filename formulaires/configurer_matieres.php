<?php
/* Formulaire de config minimal */


if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('inc/config');

function formulaires_configurer_matieres_saisies(){

    $saisies = array();
    $listObjet = lister_tables_objets_sql();
    $dataListObjet = array();

    # Generer la liste des objets disponibles
    if ( is_array($listObjet) && !empty($listObjet) )
        foreach ( $listObjet as $k => $v )
            $dataListObjet[$v['type']] = ( $v['texte_objets'] ? _T($v['texte_objets']) : $v['type'] ) . ' (' . $k . ')';

    # Genrerer les saisies
    $saisies = array(
        array(
            'saisie' => 'checkbox',
            'options' => array(
                'nom' => 'liaisons',
                'label' => _T('matieres:label_liaison_objet'),
                'explication' => _T('matieres:explication_liaison_checkbox_objet'),
                'conteneur_class' => 'editer_texte',
                'datas' => $dataListObjet
            )
        )
    );

    return $saisies;
}

/*
 * Fonction minimale pour un formulaire de configuration.
 *
 * la vue HTML (lier_matieres.html) doit exister mais peut rester vide. (_saisies)
 * Spip enregistre auto-magiquement les valeurs saisies dans une meta "matieres"
 * ( convention de nommage: formulaires_(configurer_matieres)_charger )
 */
function formulaires_configurer_matieres_charger(){
    return lire_config('matieres') ?: array();
}