<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

function matieres_affiche_milieu($flux){

    $html = '';
    $e = trouver_objet_exec($flux['args']['exec']);

    # Association de matieres sur d'autre objet (cf: ecrire/?exec=configurer_matieres );
    if (
        isset($e['type'])
        && !$e['edition']
        && isset($GLOBALS['meta']['matieres'])
        && in_array($e['type'],lire_config('matieres/liaisons'))
    ) {
        $html = recuperer_fond('prive/inclure/lier_matiere',
            array(
                'objet' => $e['type'],
                'id_objet' => $flux['args'][$e['id_table_objet']]
            )
        );

        if ( $p=strpos($flux['data'],"<!--affiche_milieu-->") )
            $flux['data'] = substr_replace($flux['data'],$html,$p,0);
    }

    return $flux;
}